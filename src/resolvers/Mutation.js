const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../utils')

const signup = async (parent, args, context, info) => {
  const password = await bcrypt.hash(args.password, 10)
  const user = await context.prisma.createUser({ ...args, password })
  const token = jwt.sign({ userId: user.id }, APP_SECRET)
  console.log(user)
  return { token, user }
}

const login = async (parent, args, context, info) => {
  const user = await context.prisma.user({ email: args.email })
  if (!user) { throw new Error('No such user found') }
  const valid = await bcrypt.compare(args.password, user.password)
  if (!valid) { throw new Error('Invalid password') }
  const token = jwt.sign({ userId: user.id }, APP_SECRET)
  console.log(token)
  return { token, user }
}

const postArticle = async (parent, args, context, info) => {
  const userId = getUserId(context)
  const { title, category, image, sections } = args
  const articleExist = await context.prisma.$exists.article({ title })
  if (articleExist) { throw new Error(`L'article "${title}" existe d�j�.`) }
  const article = await context.prisma.createArticle({
    title,
    category,
    image,
    postedBy: {
      connect: {
        id: userId
      }
    },
    sections: {
      create: sections
    }
  })
  return article
}

const updateArticle = async (parent, args, context, info) => {
  getUserId(context) // authentification
  const { prisma } = context
  const { id, category, title, image, sections } = args
  sections.forEach(async section => {
    const { id, title, text } = section
    await prisma.updateSection({
      data: {
        title, text
      },
      where: {
        id
      }
    })
  })
  const article = prisma.updateArticle({
    data: {
      category,
      title,
      image
    },
    where: {
      id
    }
  })
  return article
}

const deleteArticle = async (parent, args, context, info) => {
  getUserId(context) // authentification
  const deleted = await context.prisma.deleteArticle({ id: args.id })
  return deleted
}

module.exports = { signup, login, postArticle, deleteArticle, updateArticle }
