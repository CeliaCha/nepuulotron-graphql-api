const feed = async (parent, args, context, info) => {
  const where = args.filter ? {
    OR: [
      { title_contains: args.filter },
      { category_contains: args.filter },
      { sections_some: {
        OR: [
          { title_contains: args.filter },
          { text_contains: args.filter }
        ]
      } }
    ] } : {}
  const articles = await context.prisma.articles({
    where,
    skip: args.skip,
    first: args.first,
    orderBy: args.orderBy
  })
  const count = await context.prisma
    .articlesConnection({ where })
    .aggregate()
    .count()
  return { articles, count }
}

const getArticle = async (parent, args, context, info) => {
  const article = await context.prisma.article({ id: args.id })
  return article
}

module.exports = { feed, getArticle }
