const article = (parent, args, context) => context.prisma.section({ id: parent.id }).article()

module.exports = { article }
