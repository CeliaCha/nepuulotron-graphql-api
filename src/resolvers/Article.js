const postedBy = (parent, args, context) => context.prisma.article({ id: parent.id }).postedBy()
const sections = (parent, args, context) => context.prisma.article({ id: parent.id }).sections()

module.exports = { postedBy, sections }
