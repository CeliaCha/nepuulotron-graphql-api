const articles = (parent, args, context) => context.prisma.user({ id: parent.id }).articles()

module.exports = { articles }
