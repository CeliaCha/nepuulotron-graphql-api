"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "Section",
    embedded: false
  },
  {
    name: "Article",
    embedded: false
  },
  {
    name: "User",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `https://heroku-nepuulotron-79ab01db4e.herokuapp.com/nepuulotron-api/dev`
});
exports.prisma = new exports.Prisma();
